---
title: Why Go?
date: 2018-12-26T06:30:01.000+00:00
image: images/blog/blog-img-5.jpg
type: post

---
I would have to say Rob Pike says it best here.

{{< youtube cQ7STILAS0M >}}

I would say that it is a complete pleasure writing Go.  The idea that the types are
somthing you can just reuse or borrow is amazing.  Here is an example.
{{< highlight go "linenos=table,hl_lines=8 15-17,linenostart=1" >}}
type gopher struct{
Name string
}
{{< / highlight >}}
![Example image](https://img.icons8.com/color/48/000000/golang.png)

You can store data in this type or add behavior.
If you wanna add behavior you just add a method to it like so.

{{< highlight go "linenos=table,hl_lines=8 15-17,linenostart=1" >}}
func (g gopher) coding(){}
{{< / highlight >}}

Now your gopher type can do coding.  But your type also can store data.
This happens at runtime and you can do this by the following.
{{< highlight go "linenos=table,hl_lines=8 15-17,linenostart=1" >}}
g:=gopher{Name:"jim"}
{{< / highlight >}}

Now our type has a name "jim".
We can package jim have him in his own file.
{{< highlight go "linenos=table,hl_lines=8 15-17,linenostart=1" >}}
package main

type gopher struct{
Name string
}

func (g gopher) coding(){}

func writecode(){
g:=gopher{Name:"jim"}
}
{{< / highlight >}}

Now jim can code, has a name, and lives in a package.

{{< awesome fa fa-accusoft >}}